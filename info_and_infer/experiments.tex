\section{Simulations} \label{sec:experiments}
\begin{figure}[!t]
\begin{center}
\includegraphics{figs/simulation_example.eps} 
%\input{simulation/simulation_example.tex}
\caption{The damped Doppler function (solid gray) and typical realizations of the estimators 
under different noise levels ($n=500$, 5000, and 50000). 
Three estimators are used: the blockwise James-Stein estimator (dashed black),
and two quantized estimator with budgets of 5 bits (dashed red) and 30 bits (dashed blue).
%The 5-bit budget appears to be ``sufficient'' in the first setting but ``insufficient'' in the latter two,
%while the 30-bit one changes from ``over-sufficient'' to
%``sufficient'' and finally ``insufficient.''}
}
\label{fig:sim1}
\end{center}
\end{figure}
\begin{figure}[!t]
\begin{center}
\includegraphics{figs/simulation_rate.eps} 
%\input{simulation/simulation_rate.tex}
\caption{Risk versus effective sample size $n=1/\eps^2$
for estimating the damped Doppler function with different estimators. 
The dashed line represents the risk of the blockwise James-Stein estimator, and the solid ones
are for the quantized estimators with different budgets. 
The budgets are 5, 10, 15, 20, 25, and 30 bits, corresponding to the lines from top to bottom.
The two plots are the same curves on the original scale and the log-log scale.
}
\label{fig:sim2}
\end{center}
\end{figure}

Here we illustrate the performance of the proposed quantized estimation scheme. 
We use the function 
\[
f(x)=\sqrt{x(1-x)}\sin\left(\frac{2.1\pi}{x+0.3}\right),\quad 0\leq x\leq 1,
\]
which we shall refer to as the ``damped Doppler function,'' shown
in Figure \ref{fig:sim1} (the gray lines).  Note that the value 
$0.3$ differs from the value $0.05$ in the usual Doppler 
function used to illustrate spatial adaptation of methods such
as wavelets. Since we do not address spatial adaptivity in this
paper, we ``slow'' the oscillations of the Doppler function near zero
in our illustrations.

We use this $f$ as the underlying true mean function and generate our data according to the corresponding white noise model (\ref{eqn:wnm}),
\[
dX(t)=f(t)dt+\varepsilon dW(t), \quad 0\leq t\leq 1.
\]
We apply the blockwise James-Stein estimator,
as well as the proposed quantized estimator with different communication budgets.
We also vary the noise level $\varepsilon$ and, equivalently, the effective sample size $n=1/\varepsilon^2$.

We first show in Figure \ref{fig:sim1} some typical realizations of
these estimators on data generated under different noise levels
($n=500$, 5000, and 50000 respectively).  To keep the plots succinct,
we show only the true function, the blockwise James-Stein estimates
and quantized estimates using total bit budgets of 5 and 30 bits.  We
observe, in the first plot, that both quantized estimates deviate from
the true function, and so does the blockwise James-Stein estimates.
This is when the noise is relatively large and any quantized estimate
performs poorly, no matter how large a budget is given.  Both 5
bits and 30 bits appear to be ``sufficient/over-sufficient'' here.  In
the second plot, the blockwise James-Stein estimate is close to the
quantized estimate with a budget of 30 bits, while with a
budget of 5 bits it fails to capture the fluctuations of the true
function.  Thus, a budget of 30 bits is still ``sufficient,'' but 5
bits apparently becomes ``insufficient.''  In the third plot, the
blockwise James-Stein estimate gives a better fit than the two
quantized estimates, as both budgets become ``insufficient'' to
achieve the optimal risk.

Next, in Figure \ref{fig:sim2} we plot the risk as
a function of sample size $n$, averaging 
over 2000 simulations. Note that the bottom plot is the just the first plot on a
log-log scale.  In this set of plots, we are able to observe the phase
transition for the quantized estimators.  For relatively small values
of $n$, all quantized estimators yield a similar error rate,
with risks that are close to (or even smaller than) that
of the blockwise James-Stein estimator.  This is the
over-sufficient regime---even the smallest budget suffices to achieve
the optimal risk.  As $n$ increases, the curves start to 
separate, with estimators having smaller bit budgets leading to worse risks
compared to the blockwise James-Stein estimator, and compared
to estimators with larger budgets.  This can be seen as the sufficient regime for 
the small-budget estimators---the risks are still going down, but at a slower
rate than optimal.  The six quantized estimators all end up
in the insufficient regime---as $n$ increases, their risks begin to flatten out,
while the risk of the blockwise James-Stein estimator
continues to decrease.

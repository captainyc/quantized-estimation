\section{Introduction}

In this paper we introduce a minimax framework for nonparametric
estimation under storage constraints.  In the classical statistical
setting, the minimax risk  
%$$ R_n(\F) = \inf_{\hat f_n} \sup_{f\in \F}  R(\hat f_n, f)$$
for estimating a function $f$ from a function class $\F$
using a sample of size $n$ places no constraints on the estimator
${\hat f_n}$, other than requiring it 
to be a measurable function of the data.  However, if
the estimator is to be constructed with restrictions
on the computational resources used,
it is of interest to understand how the error can degrade.
Letting $C(\hat f_n) \leq B_n$ indicate that the
computational resources $C(\hat f_n)$ used to construct $\hat f_n$ are
required to fall within a budget $B_n$, the constrained minimax risk is
$$ R_n(\F,B_n) = \inf_{\hat f_n: C(\hat f_n)\leq B_n} \sup_{f\in \F}
R(\hat f_n, f).$$ Minimax lower bounds on the
risk as a function of the computational budget thus determine a
feasible region for computation constrained estimation, and a Pareto
optimal tradeoff for risk versus computation as $B_n$ varies.  

Several recent papers have presented results on tradeoffs between statistical
risk and computational resources, measured in terms of either running time of the algorithm,
number of floating point operations, or number of bits used to store or construct
the estimators \cite{chandrasekaran2013computational, bruer2014time,
lucic2015tradeoffs}. However, the existing work quantifies the
tradeoff by analyzing the statistical and computational performance
of specific procedures, rather than by establishing lower bounds and
a Pareto optimal tradeoff. In this paper we treat
the case where the complexity $C(\hat f_n)$ is measured by the
storage or space used by the procedure and sharply characterize the optimal tradeoff.
Specifically, we limit the
number of bits used to represent the estimator $\hat f_n$.  We focus
on the setting of nonparametric regression under standard smoothness
assumptions, and study how the excess risk depends on the storage
budget $B_n$.


We view the study of quantized estimation as a theoretical problem of
fundamental interest. But quantization may arise naturally in future
applications of large scale statistical estimation.  For instance,
when data are collected and analyzed on board a remote satellite, the
estimated values may need to be sent back to Earth for further
analysis. To limit communication costs, the estimates can be
quantized, and it becomes important to understand what, in principle,
is lost in terms of statistical risk through quantization.  A related
scenario is a cloud computing environment where data are processed for
many different statistical estimation problems, with the estimates
then stored for future analysis. To limit the storage costs, which
could dominate the compute costs in many scenarios, it is of interest
to quantize the estimates, and the quantization-risk tradeoff again
becomes an important concern.  Estimates
are always quantized to some degree in practice.  But to impose energy
constraints on computation, future processors may limit precision in
arithmetic computations more significantly \cite{galal2011energy}; the cost
of limited precision in terms of statistical risk must then be
quantified.
A related problem is to distribute the
estimation over many parallel processors, and to then limit the
communication costs of the submodels to the central host.  
We focus on the centralized setting in the current paper,
but an extension to the distributed case may be possible
with the techniques that we introduce here.

We study risk-storage tradeoffs in the
normal means model of nonparametric estimation assuming the target
function lies in a Sobolev space.  The problem is intimately related
to classical rate distortion theory \cite{gallager1968information}, and our
results rely on a marriage of minimax theory and rate distortion
ideas.  We thus build on and refine the connection between
function estimation and lossy source coding that was elucidated in
David Donoho's 1997 Wald Lectures \cite{donoho1997wald}.

We work in the Gaussian white noise model
\begin{equation}
dX(t)=f(t)dt+\varepsilon dW(t), \quad 0\leq t\leq 1,\label{eqn:wnm}
\end{equation}
where $W$ is a standard Wiener process on $[0,1]$, 
$\varepsilon$ is the standard deviation of the noise,
and $f$ lies in the periodic Sobolev space $\tilde W(m,c)$ of order $m$ and radius
$c$. (We discuss the nonperiodic Sobolev space $W(m,c)$ in
Section~\ref{sec:achievability}.) 
The white noise model is a centerpiece of nonparametric estimation.
It is asymptotically equivalent to nonparametric regression \cite{brown1996asymptotic}
and density estimation \cite{nussbaum1996asymptotic}, and simplifies
some of the mathematical analysis in our framework.
In this classical setting,
the minimax risk of estimation 
\begin{equation*}
R_\varepsilon(m,c)=\inf_{\hat f_\varepsilon}\sup_{f\in\tilde W(m,c)}\E\|f-\hat f_\varepsilon\|_2^2
\end{equation*}
is well known to satisfy
\begin{equation}
\lim_{\varepsilon\to 0}\varepsilon^{-\frac{4m}{2m+1}}R_\varepsilon(m,c)=
\left(\frac{c^2(2m+1)}{\pi^{2m}}\right)^{\frac{1}{2m+1}}\left(\frac{m}{m+1}\right)^{\frac{2m}{2m+1}}
\triangleq \mathsf P_{m,c}\label{pinsker}
\end{equation}
where $\mathsf P_{m,c}$ is Pinsker's constant \cite{nussbaum1999minimax}.  The
constrained minimax risk for quantized estimation becomes
\begin{equation*}\label{eqn_minimaxdef}
R_\varepsilon(m,c,B_\eps)=\inf_{\hat f_\varepsilon, C(\hat
f_\varepsilon)\leq B_\eps}\sup_{f\in\tilde W(m,c)}\E\|f-\hat f_\varepsilon\|_2^2
\end{equation*}
where $\hat f_\eps$ is a \textit{quantized estimator} that is
required to use storage $C(\hat f_\eps)$ no greater than $B_\eps$ bits in
total. Our main result identifies three separate quantization
regimes. 

\begin{itemize}
\vskip10pt
\item In
the \textit{over-sufficient regime}, the number of bits
is very large, satisfying $B_\eps \gg \eps^{-\frac{2}{2m+1}}$ and the classical minimax rate
of convergence $R_\eps \asymp \eps^{\frac{4m}{2m+1}}$ is obtained.
Moreover, the optimal constant is the Pinsker constant ${\mathsf
P}_{m,c}$.  


\vskip10pt
\item In the \textit{sufficient regime}, the number
of bits scales as $B_\eps \asymp \eps^{-\frac{2}{2m+1}}$.  This level
of quantization is just sufficient to preserve the classical minimax
rate of convergence, and thus in this regime
$R_\varepsilon(m,c,B_\eps) \asymp \eps^{\frac{4m}{2m+1}}$.
However, the optimal constant degrades to a new constant ${\mathsf
P}_{m,c} + {\mathsf Q}_{m,c,d}$, where ${\mathsf Q}_{m,c,d}$ is 
characterized in terms of the solution of a certain variational
problem, depending on $d=\lim_{\eps\rightarrow 0}
B_{\eps} \eps^{\frac{2}{2m+1}}$.

\vskip10pt
\item In the \textit{insufficient regime}, the number
of bits scales as $B_\eps \ll \eps^{-\frac{2}{2m+1}}$, with however
$B_\eps \to \infty$.  Under this scaling
the number of bits is insufficient to preserve the unquantized
minimax rate of convergence, and the quantization error dominates
the estimation error.   We show that the quantized
minimax risk in this case satisfies
\begin{equation*}
\lim_{\varepsilon\to 0} B_\eps^{2m} R_\varepsilon(m,c,B_\eps) = 
\frac{c^2 m^{2m}}{\pi^{2m}}.
\end{equation*}
Thus, in the insufficient regime the
quantized minimax rate of convergence is $B_\eps ^{-2m}$,
with optimal constant as shown above.
\end{itemize}

By using an upper bound for the family of constants ${\mathsf Q}_{m,c,d}$,
the three regimes can be combined together to view 
the risk in terms of a decomposition into estimation error and quantization error.
Specifically, we can write
\begin{equation*}
R_\varepsilon(m,c,B_\varepsilon)\;\; \approx \underbrace{\mathsf P_{m,c}\,\varepsilon^{\frac{4m}{2m+1}}}_{\mbox{\footnotesize estimation error}}
+ \underbrace{\frac{c^2m^{2m}}{\pi^{2m}}B_\varepsilon^{-2m}}_{\mbox{\footnotesize quantization error}}.
\end{equation*}
When $B_\varepsilon\gg\varepsilon^{-\frac{2}{2m+1}}$, the estimation error dominates the quantization error, 
and the usual minimax rate and constant are obtained.
In the insufficient case $B_\varepsilon\ll\varepsilon^{-\frac{2}{2m+1}}$,
only a slower rate of convergence is achievable. When $B_\varepsilon$ and $\varepsilon^{-\frac{2}{2m+1}}$ are comparable, 
the estimation error and quantization error are on the same order.
The threshold $\varepsilon^{-\frac{2}{2m+1}}$ should not be surprising,
given that in classical unquantized estimation the minimax rate
of convergence is achieved by estimating the first $\varepsilon^{-\frac{2}{2m+1}}$ Fourier
coefficients and simply setting the remaining coefficients to zero.
This corresponds to selecting a smoothing bandwidth that scales as
$h\asymp n^{-\frac{1}{2m+1}}$ with the sample size $n$.


% rough, high level overview of proof
% outline of remainder of paper

At a high level, our proof strategy integrates elements of minimax
theory and source coding theory.  In minimax analysis one computes
lower bounds by thinking in Bayesian terms to look for least-favorable
priors.  In source coding analysis one constructs worst case
distributions by setting up an optimization problem based on mutual
information.  Our quantized minimax analysis requires that these
approaches be carefully combined to balance the estimation and
quantization errors. To show achievability of the lower bounds we
establish, we likewise need to construct an estimator and coding
scheme together.  Our approach is to quantize the blockwise
James-Stein estimator, which achieves the classical Pinsker bound.
However, our quantization scheme differs from the approach taken in
classical rate distortion theory, where the generation of the codebook
is determined once the source distribution is known.  In our setting,
we require the allocation of bits to be adaptive to the data, using
more bits for blocks that have larger signal size.  We therefore
design a quantized estimation procedure that adaptively distributes
the communication budget across the blocks.  Assuming only 
a lower bound $m_0$ on the smoothness $m$ and an upper bound $c_0$ on the radius $c$ of the Sobolev
space, our quantization-estimation procedure is
adaptive to $m$ and $c$ in the usual statistical sense,
and is also adaptive to the coding regime.  In other words, 
given a storage budget $B_\eps$, the coding procedure
achieves the optimal rate and constant for the unknown
$m$ and $c$, operating in the corresponding regime for those
parameters.


In the following section we establish some notation, outline our proof
strategy, and present some simple examples.  In
Section~\ref{sec:lowerbound} we state and prove our main result on
quantized minimax lower bounds, relegating some of the technical
details to an appendix. In Section~\ref{sec:achievability} we show
asymptotic achievability of these lower bounds, using a quantized
estimation procedure based on adaptive James-Stein estimation and
quantization in blocks, again deferring proofs of technical lemmas to
the supplementary material. This is followed by a presentation of some
results from experiments in Section~\ref{sec:experiments},
illustrating the performance and properties of the proposed quantized
estimation procedure.


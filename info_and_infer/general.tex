\section{Quantized estimation and minimax risk} 
\label{sec:general}
\def\given{\,|\,}


Suppose that $(X_1,\dots, X_n)\in\mathcal X^n$ is
a random vector drawn from a distribution $P_n$. Consider the problem of estimating a functional
$\theta_n=\theta(P_n)$ of the distribution, assuming $\theta_n$ is
restricted to lie in a parameter space $\Theta_n$.   To unclutter some
of the notation, we will suppress the subscript $n$ and write $\theta$
and $\Theta$ in the following, keeping in mind that nonparametric settings
are allowed.  The subscript $n$ will be maintained for random variables.
The minimax $\ell_2$ risk 
of estimating $\theta$ is then defined as
\begin{equation*}
R_n(\Theta)=\inf_{\hat\theta_n}\sup_{\theta\in\Theta}\mathbb E_\theta\|\theta-\hat\theta_n\|^2
\end{equation*}
where the infimum is taken over all possible estimators
$\hat\theta_n:\mathcal X^n\to\Theta$ that are measurable
with respect to the data $X_1,\dots,X_n$. 
We will abuse notation by using $\hat\theta_n$ to denote both the estimator 
and the estimate calculated based on an observed set of data.
Among numerous approaches to
obtaining the minimax risk,
the Bayesian method is best aligned with quantized estimation.  
Consider a prior distribution $\pi(\theta)$ whose support is a subset of $\Theta$. Let $\delta(X_{1:n})$
be the posterior mean of $\theta$ given the data $X_1,\dots,X_n$, which minimizes the integrated
risk. Then for any estimator $\hat\theta_n$, 
\begin{equation*}
\sup_{\theta\in\Theta}\mathbb E_\theta\|\theta-\hat\theta_n\|^2\geq\int_\Theta\mathbb E_\theta\|\theta-\hat\theta_n\|^2 d\pi(\theta)\geq  \int_\Theta\mathbb E_\theta\|\theta-\delta(X_{1:n})\|^2 d\pi(\theta).
\end{equation*}
Taking the infimum over $\hat\theta_n$ yields
\begin{equation*}
\inf_{\hat\theta_n}\sup_{\theta\in\Theta}\mathbb
E_\theta\|\theta-\hat\theta_n\|^2\geq\int_\Theta\mathbb
E_\theta\|\theta-\delta(X_{1:n})\|^2 d\pi(\theta)\triangleq
R_n(\Theta;\pi).
\end{equation*}  
Thus, any prior distribution supported on $\Theta$
gives a lower bound on the minimax risk, and selecting the
least-favorable prior leads to the largest lower bound provable by
this approach.

Now consider constraints on the storage or
communication cost of our estimate.
We restrict to the set of estimators that
use no more than a total of $B_n$ bits; 
that is, the estimator takes
at most $2^{B_n}$ different values. 
Such \textit{quantized estimators} can be formulated by the following two-step procedure.
First, an \emph{encoder} maps 
the data $X_{1:n}$ to an index $\phi_n(X_{1:n})$, where
\begin{equation*}
\phi_n:\mathbb {\mathcal X}^n\to\{1,2,\dots,2^{B_n}\}%\triangleq\mathbb C(B_n)
\end{equation*}
is the \emph{encoding function}.
The \emph{decoder}, after receiving or retrieving the index, 
represents the estimates based on a \emph{decoding function}
\begin{equation*}
\psi_n:\{1,2,\dots,2^{B_n}\}\to \Theta,
\end{equation*}
mapping the index to a codebook of estimates.  All that needs to be
transmitted or stored is the $B_n$-bit-long index, and the quantized
estimator $\hat\theta_n$ is simply $\psi_n\circ\phi_n$, the
composition of the encoder and the decoder functions.  Denoting by
$C(\hat\theta_n)$ the storage, in terms of the number of bits,
required by an estimator $\hat\theta_n$, the minimax risk of quantized
estimation is then defined as
\begin{equation*}
R_n(\Theta, B_n)=\inf_{\hat\theta_n,C(\hat\theta_n)\leq B_n}\sup_{\theta\in\Theta}\mathbb E_\theta\|\theta-\hat\theta_n\|^2,
\end{equation*}
and we are interested in the effect of the constraint on the minimax risk. 
%\comment{Try to avoid using $Q$ and $\mathcal Q$, and be consistent with (\ref{eqn_minimaxdef}). Also, changed $\check\theta$ to $\hat\theta$ so that we can reserve $\check\theta$ for the quantized estimator used to prove achievability.}}
Once again, we consider a prior distribution $\pi(\theta)$ supported
on $\Theta$ and let $\delta(X_{1:n})$ be the posterior mean of
$\theta$ given the data. The integrated risk can then be decomposed as
\begin{equation}
\label{decomp1}\begin{aligned}
\int_\Theta\mathbb E_\theta\|\theta-\hat\theta_n\|^2d\pi(\theta)&=\mathbb E\|\theta-\delta(X_{1:n})+\delta(X_{1:n})-\hat\theta_n\|^2\\
&=\mathbb E\|\theta-\delta(X_{1:n})\|^2+\mathbb E\|\delta(X_{1:n})-\hat\theta_n\|^2
\end{aligned}\end{equation}
where the expectation is with respect to the joint distribution of
$\theta\sim\pi(\theta)$ and $X_{1:n} \given \theta\sim P_\theta$, and the second equality is due to 
\begin{align*}
\nonumber
&\mathbb
E\langle\theta-\delta(X_{1:n}),\delta(X_{1:n})-\hat\theta_n\rangle\\
&=\mathbb E\left(\mathbb E\left(\langle\theta-\delta(X_{1:n}),\delta(X_{1:n})-\hat\theta_n\rangle \given X_{1:n}\right)\right)\\
%&=\mathbb E\left(\langle\mathbb E(\theta-\delta(X_{1:n}) \given X_{1:n}),\mathbb E(\delta(X_{1:n})-\hat\theta_n \given X_{1:n})\rangle\right)\\
%&=\mathbb E\left(\langle0,\mathbb E(\delta(X_{1:n})-\hat\theta_n \given X_{1:n})\rangle\right)=0,
&=\mathbb E\left(\langle\mathbb E(\theta-\delta(X_{1:n}) \given X_{1:n}),\delta(X_{1:n})-\hat\theta_n \rangle\right)\\
&=\mathbb E\left(\langle0,\delta(X_{1:n})-\hat\theta_n\rangle\right)=0,
\end{align*}
using the fact that $\theta\to X_{1:n}\to \hat\theta_n$ forms
a Markov chain. The first term in the decomposition (\ref{decomp1}) is
the Bayes risk $R_n(\Theta;\pi)$. 
The second term can be viewed as the excess risk due to quantization. 

Let $T_n=T(X_1,\dots,X_n)$ be a sufficient statistic for $\theta$. The
posterior mean can be expressed in terms of $T_n$ and we will abuse 
notation and write it as $\delta(T_n)$. Since the quantized
estimator $\hat\theta_n$ uses at most $B_n$ bits, we have
\begin{equation*}
B_n\geq H(\hat\theta_n)\geq H(\hat\theta_n)-H(\hat\theta_n
\given \delta(T_n))=I(\hat\theta_n;\delta(T_n)),
\end{equation*}
where $H$ and $I$ denote the Shannon entropy and mutual information, respectively.
Now consider the optimization
\begin{align*}
\inf_{P(\cdot \given \delta(T_n))}\ &\ \mathbb E\|\delta(T_n)-\tilde\theta_n\|^2\\
\text{such that}\ &\ I(\tilde\theta_n;\delta(T_n))\leq B_n
\end{align*}
where the infimum is over all conditional distributions
$P(\tilde\theta_n \given \delta(T_n))$. 
This parallels the definition of the distortion rate function,
minimizing the distortion under a constraint on mutual information
\cite{gallager1968information}. 
Denoting the value of this optimization by $Q_n(\Theta, B_n;\pi)$, we can lower bound
the quantized minimax risk by
\begin{equation*}
R_n(\Theta, B_n)\geq R_n(\Theta;\pi)+Q_n(\Theta, B_n;\pi).
\end{equation*}
Since each prior distribution $\pi(\theta)$ supported on $\Theta$ gives a
lower bound, we have
\begin{equation*}\label{eqn_generallowerbound}
R_n(\Theta, B_n)\geq \sup_\pi \Bigl\{R_n(\Theta;\pi)+Q_n(\Theta, B_n;\pi)\Bigr\}
\end{equation*}
and the goal becomes to obtain a least favorable prior for the
quantized risk.

Before turning to the case of quantized estimation
over Sobolev spaces, we illustrate this technique on some simpler,
more concrete examples.

\begin{example}[Normal means in a hypercube]
Let $X_i\sim \mathcal N(\theta,\sigma^2I_d)$ for $i=1,2,\dots,n$. Suppose that $\sigma^2$ is known and $\theta\in[-\tau,\tau]^d$ is to be estimated. We choose the prior $\pi(\theta)$ on $\theta$ to be a product distribution with density
\begin{equation*}
\pi(\theta)=\prod_{j=1}^d \frac{3}{2\tau^3}{\left(\tau-|\theta_j|\right)_+}^2.
\end{equation*}
It is shown in \cite{johnstone2015gaussian} that 
\begin{equation*}
R_n(\Theta;\pi)\geq \frac{\sigma^2
  d}{n}\frac{\tau^2}{\tau^2+12\sigma^2/n}\geq
c_1\frac{\sigma^2 d}{n}
\end{equation*}
where $c_1=\frac{\tau^2}{\tau^2+12\sigma^2}$. Turning to
$Q_n(\Theta,B_n;\pi)$, let $T^{(n)}=(T^{(n)}_1,\dots,T^{(n)}_d)=\mathbb E(\theta|X_{1:n})$ be the 
posterior mean of $\theta$. In fact, by the independence and symmetry among
the dimensions, we know $T_1,\dots,T_d$ are independently and identically 
distributed. Denoting by $T_0^{(n)}$ this common distribution, we have 
\begin{equation*}
Q_n(\Theta,B_n;\pi)\geq d\cdot q(B_n/d)
\end{equation*}
where $q(B)$ is the distortion rate function for $T^{(n)}_0$, i.e.,
the value of the following problem
\begin{align*}
\inf_{P(\hat T\given T_0^{(n)})}\ &\ \mathbb E(T_0^{(n)}-\hat T)^2\\
\text{such that}\ &\ I(\hat T;T_0^{(n)})\leq B.
\end{align*}
Now using the Shannon lower bound \cite{cover2006elements}, we get 
\begin{equation*}
Q_n(\Theta,B_n;\pi)\geq  \frac{d}{2\pi e}\cdot 2^{h(T_0^{(n)})}\cdot 2^{-\frac{2B_n}{d}}.
\end{equation*}
Note that as $n\to\infty$, $T_0^{(n)}$ converges to $\theta$ in distribution, so there exists 
a constant $c_2$ independent of $n$ and $d$ such that
\begin{equation*}
R_n(\Theta,B_n)\geq c_1\frac{\sigma^2 d}{n}+c_2d\,2^{-\frac{2B_n}{d}}.
\end{equation*}
This lower bound intuitively shows the risk is regulated by two
factors, the estimation error and the quantization error; whichever is
larger dominates the risk.   The scaling behavior of this lower bound
(ignoring constants) can be achieved by first quantizing each of the $d$ intervals
$[-\tau,\tau]$ using $B_n/d$ bits each,
and then mapping the {\sc mle} to its closest codeword. 
%The details of this calculation are given in the Appendix.
\end{example}

%\begin{example}[Binomial]
%Let $X_i\sim \text{Bern}(\theta)$ be independent samples from a
%Bernoulli distribution,
%for $i=1,2,\ldots, n$, and take $\pi(\theta) = 1$ to be the uniform prior on $[0,1]$. Then $T_n =
%\overline X_n$ and 
%\begin{equation*}
%\delta(T_n) = \frac{n}{n+2} \overline X_n + \frac{2}{n+2} \cdot \frac{1}{2}
%\end{equation*}
%with $I(\tilde\theta_n, \delta(T_n)) = I(\tilde\theta_n, \overline X_n)$.
%In this case it can be shown that
%\begin{equation*}
%R_n(\Theta,B_n)\geq \frac{c_1}{n} +
%c_2 H^{-1}\left( 1- \frac{B_n}{n}\right)
%\end{equation*}
%for constants $c_1$ and $c_2$, where $H^{-1}$ is the inverse of
%the binary entropy function on $[0,\frac{1}{2}]$.
%\end{example} 

\begin{example}[Gaussian sequences in Euclidean balls] 
In the example shown above, the lower bound is tight only in terms of
the scaling of the key parameters.  In some instances, we are able to find
an asymptotically tight lower bound for which we can show
achievability of both the rate and the constants. Estimating the mean vector of a Gaussian
sequence with an $\ell_2$ norm constraint on the mean is one of such
case, as we showed in previous work \cite{zhu2014quantized}.

Specifically, let $X_i \sim \mathcal N(\theta_i, \sigma^2_n)$ for $i=1,2,\ldots,
n$, where $\sigma^2_n = \sigma^2/n$.   Suppose that the parameter
$\theta = (\theta_1,\ldots, \theta_n)$ lies in the 
Euclidean ball $\Theta_n(c) = \left\{\theta : \sum_{i=1}^n \theta_i^2 \leq
c^2\right\}$.  Furthermore, suppose that $B_n = nB$.  Then using the prior $\theta_i \sim \mathcal N(0,c^2)$ it can be
shown that 
\begin{equation*}
\liminf_{n\to\infty} R_n(\Theta_n(c), B_n) \geq \frac{\sigma^2
  c^2}{\sigma^2 + c^2} + \frac{c^4 2^{-2B}}{\sigma^2 + c^2} .
\end{equation*}
The asymptotic estimation error
$\sigma^2 c^2 / (\sigma^2 + c^2)$ is the well-known Pinsker
bound for the Euclidean ball case.   
As shown in \cite{zhu2014quantized}, an explicit quantization scheme can be constructed that asymptotically
achieves this lower bound, realizing the smallest possible
quantization error $c^4 2^{-2B} / (\sigma^2 + c^2)$ for a budget of
$B_n = nB$ bits.

The Euclidean ball case is clearly relevant to the Sobolev ellipsoid
case, but new coding strategies
and proof techniques are required.  In particular, as will
be made clear in the sequel, we will use an adaptive allocation of bits
across blocks of coefficients, using more bits for blocks that have
larger estimated signal size.  Moreover, determination of the optimal
constants requires a detailed analysis of the worst case prior
distributions and the solution of a series of variational problems.


\end{example} 

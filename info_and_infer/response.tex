
\documentclass[pdftex,12pt]{article}

\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{enumerate}
\usepackage{amsfonts,amsmath,amssymb,amsthm}
\usepackage{graphicx,subfigure}
\usepackage[pdftex]{color}
\usepackage{epic,eepic,eepicemu}
\usepackage{epsf}
\usepackage{epsfig}
\usepackage{fancyhdr}
\usepackage{graphics}
\usepackage{psfrag,latexsym}
\usepackage{times}
\newcommand{\thetamin}{\ensuremath{\theta^*_{min}}}
\newcommand{\mutinc}{\ensuremath{\alpha}}
\bibliographystyle{abbrv}


\setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-6cm}
\setlength{\textheight}{\paperheight}
\addtolength{\textheight}{-4cm}
\addtolength{\textheight}{-1.1\headheight}
\addtolength{\textheight}{-\headsep}
\addtolength{\textheight}{-\footskip}
\setlength{\oddsidemargin}{0.5cm}
\setlength{\evensidemargin}{0.5cm}
\def\P{{\mathbb P}}
\def\E{{\mathbb E}}
\def\degmax{d}
\def\pdim{p}
\def\numobs{n}
\let\hat\widehat
\definecolor{blue}{rgb}{0.01,0.01,0.75}
\def\rc#1{{\it\textcolor{blue}{#1}}\smallskip}
\def\rskip{\vskip5pt}
\def\argmin{\mathop{\rm arg\,min}}
\parindent0pt
\parskip12pt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\baselinestretch}{1.02}

\begin{document}

\vspace*{5pt}

We thank the three reviewers and the associate editor for their
thoughtful, insightful and constructive comments.  We apologize for
the delay in preparing the revision---one of us finished a
dissertation and transitioned from Chicago to Philadelphia in the
interim, and a number of issues raised by the referees required
careful attention. We have prepared a revised version of the
manuscript that addresses all of the main comments and suggestions
made by the reviewers, as well as the minor comments and corrections.
Detailed descriptions of the changes and responses to the individual
comments are given below.


\subsection*{Reviewer 1}

\rc{Overall, the paper is extremely well-written. The pages 4-5 discussing
the consequences of the results and the regimes in which quantization
affects performance and the extent to which it degrades performance is
particularly enlightening.}

We appreciate the positive feedback.

\rc{I have a few minor comments:}
\begin{enumerate}[1.]
\item \rc{While motivating the paper: You seem to want to equate "computing
resources" to number of bits used to represent the estimator. There
are two reasons I feel this is a bit of a stretch: (1) "Computing
resources" could very well be one of compute time, storage cost,
communication cost. (2) In cases when the cost of storing the
estimator is the primary consideration, it is not clear that one can
ignore the cost of storing the data itself.
\rskip
Even the connection to distributed estimation discussed in the paper
is somewhat tenuous: quantized minimax is relevant to a particular
strategy of distributed estimation (where one computes estimators
locally and communicates them). The connection between quantized
minimax and distributed estimation more generally deserves more study
in my opinion.
\rskip
Overall, my main suggestion here would be to talk briefly about other
possible interesting interpretations of the "minimax-with-constraints"
problem, and possibly restrict the brief discussion of application
scenarios to ones where the connection is absolutely clear.}

We appreciate the suggestion and have included a more detailed
discussion of ``computing resources'' and related constrained
statistical problems in the introduction of the revised manuscript.

\item \rc{The examples on pages 9-10-11: It would be nice if these could be
done more carefully (although perhaps not the main point of the paper)
-- i.e. I imagine that obtaining more precise results for these
examples might not be too hard. You use the phrase "It can be shown
that..." several times; I would include a citation or a short
explanation.}

We have included details of the derivations in the examples,
and have removed the Bernoulli example (see response to Reviewer 3).

\item \rc{When considering the achievability in Section 4 the authors
consider a fairly complicated procedure. I think it would help to
understand why much simpler procedures -- say using a truncated series
estimator with direct quantization -- does not work. Is it a matter of
obtaining precise constants or are the rates worse for simple
procedures?}

Simpler procedures could lead to the same rate, but with worse
constants. In fact, there is a crude way of analyzing a class of
simpler procedures. Suppose that $\hat \theta=\hat\theta(X)$ is an
estimator of $\theta\in\Theta$ without quantization. To design a
$B$-bit quantized estimator, let $\check\Theta$ be a
$\delta$-covering of the parameter space $\Theta$ such that
$\log |\check\Theta|\leq B$. The quantized estimator is then defined to
be
\[
\check\theta=\check\theta(X) = \argmin_{\theta'\in\check\Theta}\|\hat\theta(X)-\theta'\|.
\]
Now the mean squared error satisfies
\begin{align*}
\E_\theta\|\check\theta-\theta\|^2&=\E_\theta\|\check\theta-\hat\theta+\hat\theta-\theta\|^2\\
&\leq 2\E_\theta\|\hat\theta-\theta\|^2+2\E_\theta\|\check\theta-\hat\theta\|^2\\
&\leq 2\sup_{\theta'}\E_{\theta'}\|\hat\theta-\theta'\|^2+2\delta^2.
\end{align*}
If we pick $\hat\theta$ to be a minimax estimator for $\Theta$, the first term here gives the minimax risk for estimating $\theta$ in the parameter space $\Theta$. We can also carefully pick a covering so that $\delta$ is the smallest among all coverings with $2^B$ elements. 
Thus, with an extra constant factor of 2, the mean squared error of this quantized estimator is decomposed into the minimax risk of estimating $\Theta$ and an error term due to quantization, which is related to the metric entropy of $\Theta$.

The reasons we consider and analyze the proposed procedure instead
of this class of simpler procedures are twofold.  First, the
proposed quantized estimator can be carefully analyzed and shown to
asymptotically achieve the information-theoretic lower bound, due to
the random codebook and the proper amount of shrinkage.  Second, the
simpler procedures are not necessarily as simple as they seem to
be. For the Sobolev space case, it is not obvious how we could easily
obtain a covering of the collections of Sobolev ellipsoids with a
given number of elements. Our proposed procedure in the paper, in some
sense, can be viewed as a way to generate such a covering. It is also
done in an adaptive fashion, without having to generate each and every
element of the covering.

We have added comments on these points to the paper.


\item \rc{Re. the comment/remark on "universal codes" on Page 27. It seems to
me that a universal code, is closely related to an epsilon-net of the
parameter space? In particular, once one requires a uniformly good
code, it seems to me like the best possible code will be an
epsilon-net, and the number of bits will be closely related to the
metric entropy of the Sobolev ellipsoid. Perhaps, a comment clarifying
the connection between metric-entropy and universal codes would be
helpful.}

This is related to our comments to the previous point.
We agree that an $\epsilon$-net or covering of $\Theta$ would give a
universal code, and it is closely related to the metric entropy of the
parameter space. However, the risk of the estimator based on this does
construction does not achieve the lower bound up to the level of constants.
We have added a comment on this point in the revision.


\end{enumerate}

\rc{Minor adjustments:}
\begin{enumerate}[1.]
\item \rc{In page 9, line 44 \texttt{--} I would suggest moving the explanation of
this bound earlier (right now it appears at the end of the example).}

We have added further detail and explanations for this example.

\item \rc{Page 10, Binomial example \texttt{--} give a citation for the fact that you
are using.}

We have removed this example; see the response to Reviewer 3.

\item \rc{Page 4, line 25-26 \texttt{--} obtains \texttt{-->} "is
  obtained"}

Changed

\item \rc{Page 9, line 44 \texttt{--} exist \texttt{-->} "exists"}

Fixed; and this section has been modified to provide more detail.

\item \rc{Page 11, line 36 \texttt{--} space \texttt{-->} "spaced"}

Corrected

\item \rc{Page 11, The white noise model is equivalent \texttt{-->} "The white noise
model is **asymptotically** equivalent"}

Changed, and we have added a reference.

\item \rc{Page 11, In this model... \texttt{-->} I would clarify that the $\epsilon$
refers to the noise level in the white-noise model introduced
earlier.}

We have clarified this.

\item \rc{Page 12, let let \texttt{-->} "let"}

Corrected

\item \rc{There are various places in which there are many large white spaces
because of the long displays of equations \texttt{--} I'd recommend
re-ordering/breaking some of the large displays up to avoid large
white spaces.}

This should be fixed in the final formatting of the journal.

\end{enumerate}

\subsection*{Reviewer 2}

\rc{The paper gives an interesting and intricate example of how to
generalize existing statistical theory to accommodate storage
trade-offs. The proofs appear to be correct, except for a few points
that are unclear and mentioned in Section 5.
While the lower bound is completely satisfactory, there are a few
shortcomings of the presented estimator. It is not computationally
feasible, which is to be expected since it sharply achieves
information theoretic bounds that would already be hard to obtain in
the case of quantizing a Gaussian random variable. Moreover, it is
only adaptive to the underlying smoothness parameters up to an upper
bound on them, contrary to the estimator they base their
generalization off of. This is due to the fact that an upper bound has
to be placed beforehand on the size of the codebook.}

Thank you for the positive feedback and detailed remarks, which are 
extremely helpful. We have revised the paper accordingly. The
detailed comments are addressed as follows.

\rc{Minor remarks}
\begin{enumerate}
\item \rc{Page 5, line 51: Insert the word ``bound'', ``a lower
  \textit{bound} $m_0$''}

Fixed.

\item \rc{Page 8, line 28: There shouldn't be an expectation around the
second term in the inner product: measurability is used to take it out
of the conditional expectation.}

Changed. 

\item \rc{Page 9, line 10 and later: Technically, what is used here is called
\textit{distortion rate function} in [CT12], because the distortion is given in
terms of the rate, not the other way round. However, rate distortion
seems to be the term that is more commonly known.}

Changed.

\item \rc{Page 9, line 48: The lower bound on $Q_n$ should come with a
reference; it is not clear how to get this without further
explanation.}

We have added further detail.

\item \rc{Page 15, Figure 1: What are $m, c$ for this particular
  figure?}

We have added a note that $m = 2$ and $c^2/\pi^{2 m} = 1$ in the
caption to this figure.

\item \rc{Page 19, line 39: It would be nice to spend a few lines explaining
why this is true. We assume the Sobolev constraint is competing with
the objective?}

We have added a reference to Cover and Thomas for this fact. 
%(It's
%perhaps not accurate to say that the Sobolev constraint is competing
%with the constraint, since the waterfilling solution just clips
%the parameters.)

\item \rc{Page 20, line 26: Is there a reference justifying the asymptotic
equivalence of the continuous formulation and the discrete one? It
seems to work similarly to the argument in [Don00, Theorem 3.1], but
there, a piecewise constant approximation is used.}

We now refer here to Nussbaum's article on Pinsker's theorem.

\item \rc{Page 21, line 44ff: How exactly is this handled numerically, since
$\alpha$ still depends on $\sigma(x)^2$ and $x_0$?}

We have added further detail.

\item \rc{Page 22, line 43: To satisfy the curiosity of the reader, it would
be helpful to mention early on the bound on $K$ in Lemma 6.}

We have added this bound, refering to Lemma 7.

\item \rc{Page 26, line 47: The fact that the estimator only achieves the
bound on the bit size asymptotically left us wondering for most of the
section why it is admissible here. It would be helpful to mention this
fact earlier. Also, the bound on $K$ from Lemma 6 is needed here as
well.}

We have added a reference to the bound on $K$ here.

\item \rc{Page 27, line 39: Is it Theorem 4.1 that's meant to be referenced
here, not Theorem 3.1?}

We have clarified that we are comparing to the lower bound in Theorem 3.1.

\item \rc{Page 30, Figure 3: It's hard to see the under- and
over-sufficiency from these plots. In particular, in the $n = 500$ case,
the 5 bits estimator already seems overly smooth, while in the $n =
50000$ case, the 30 bits estimator seems still pretty well aligned with
the J-S one.}

We have removed this comment from the caption.  The sufficiency regimes are easier to see in Figure 4.

\item \rc{Page 38, line 27 and 32: There seem to be weights $a^2_j$
  missing here.}

Corrected.

\item \rc{Page 38, line 35: It would be helpful to explain the structure of
the proof. It's not clear why we pick
$s_j = (1 - \tau) \sigma_j^2$, and the limit $\tau \to 0$ needed to finish the proof is not
mentioned anywhere.}

The logic and use of the limit $\tau\to 0$ is already mentioned;
please see p.~39 lines 19--30.

\item \rc{Page 39, line 38: Should the last statement be an inequality
instead of an equality?}

Corrected.

\item \rc{Page 40, line 20: We need that $d_n$ is bounded, which is never
mentioned. It follows quickly and can be found in [Tsy09], of which
the majority of this section is based on, but should be made explicit
here as early as possible.}

We've included a note for this.

\item \rc{Page 41, lines 11 and 14: How exactly is this applied, how is
$\max_{1\leq j < n} a^2_j s^2_j$ controlled, why is the probability in question $o(I)$,
and how is the limit $\tau \to 1$ handled? Again, most of this seems to be
analogous to [Tsy09], but needs to be mentioned.}

We have added more details for the proof. 

\item \rc{Page 42, line 37: How is this condition for the minimizer
obtained?}

It is obtained by considering the ratio of the neighboring terms indexed by $J$ and $J+1$, and comparing it to 1.

\item \rc{Page 43, line 21: $\tilde \sigma^4_j/(\tilde \sigma^2_j +
  \varepsilon^2$), square missing}

Corrected.

\item \rc{Page 43, line 47: This is not clear to us: how can L'H\^opital's
rule be applied here? Can $J_\varepsilon$ be differentiated with respect to $\varepsilon$?}

It is actually due to a Taylor's expansion of the function $\sqrt{x}$. We have included details of the calculation. 

\item \rc{Page 44, line 32: There seem to be arguments missing here. Why is
the linear constraint the only active one? Why can it be deduced from
$\int f v dx \leq 0$ and $\int g v dx \leq 0$ that $f=λg$ if $v$ does
not comprise all functions in a certain vector space, but only a
subset?}

We've included a lemma supporting the argument. 

\item \rc{Page 44, line 45: What's $b_{\max}$? It doesn't seem to be defined
anywhere.}

It's defined on p. 45 line 8.

\item \rc{Page 46, line 25: $k$ subscript missing, $\check S_k^2$}

Corrected.

\item \rc{Page 47, line 39: Is it correct that $b$ in the definition of $Z^∗$ is
not coupled to the remaining parameters? Should it be $q$?}

We made the correction $b \to q$.

\item \rc{Page 49, line 51: There are no Section numbers in the appendix, so
we have to guess what Section A.2 refers to.}

The sections were numbered but not this section; corrected.

\item \rc{Page 50, line 39: (A.2) refers to an equation in the proof of the
lower bound. The reference got probably mixed up. The same reference
also occurs later on (page 51, line 22; page 55, line 17)}

Corrected.

\item \rc{Page 53, line 31: Overloaded variable name, $b^∗$ appears
  twice.}

Note that it's \verb|b^\star| and \verb|b^*|.

\item \rc{Page54,line 11: The sum should run from $k=0$to $k=K-1$.}

Corrected.

\item \rc{Page 54, line 41: This should be an equality, not $\leq$.}

Corrected.

\item \rc{Page 55, line 30: The minimum should be over $\Omega_{mon}$,
  not $\Omega_{blk}$.}

Corrected.

\item \rc{Page 55, line 36: The sum should stop at $N$, not $K$.}

Corrected.

\item \rc{Page 56, line 14: Why is $V_\varepsilon$ continuous with
  respect to $B′$?}

This follows from the definition of the variational problem.

\item \rc{Page 57, line 22: Does this follow immediately from the results in
[Sak68]? More specifically, [Sak68, Equation (34)] pertains to a
specific value of $\zeta(t)$, given there in terms of $\varepsilon$ε. It would be helpful
to outline the steps needed to generalize this result.}

We've included a lemma (Lemma 13) generalizing the results in [Sak68], and fixed a small mistake in the proof of Lemma 7 (now Lemma 8).

\item \rc{Page 57, line 33: Why is there no square on the $\zeta(t)^{-1}$ term when
going from line 31 to line 33?} 

Corrected in the new proof of Lemma 8.

\item \rc{Page 64, line 14: Why is the sequence
  $\frac{\theta^{*4}_j}{\theta_j^{∗2}+\varepsilon^2} 2^{-2b^*_j}$
  decreasing?}

The sequence is decreasing because the optimal sequence $b^*_j$ is given by the 
``reverse water-filling'' algorithm.

\item \rc{Page 64, line 24: The mentioned statement that $b^∗$ is
 non-increasing is not shown. Also, the phrase sounds like there are
 some more things to be proved later on, while half of them have been
 proved in the preceding lines ($b^∗_1 \leq b_{\max}$).}

We've re-organized the proof and added more details.

\end{enumerate}

\subsection*{Reviewer 3}

\rc{Major comments:}
\begin{enumerate}
\item \rc{My main critics are on the applicability of this result. The proof
relies on the properties of this one-dimensional Gaussian white noise
model. Is there any evidence that in practice the estimator for this
model needs to be quantized?}

The consideration of the white noise model is mainly for mathematical
convenience, as it asymptotically equivalent to many nonparametric
estimation problems. A comment and reference has been added.
While the described scenarios are just examples, 
we can envision that quantized estimation may indeed 
see applications in the future. Nevertheless, as
we state in the paper, our abstraction of the problem 
for nonparametric regression allows us to study it formally
and sharply characterize the tradeoffs.

While quantized estimation of one-dimensional functions might not be
the most common task in practice, it is not entirely impossible in
real life, especially when a huge number of estimation problems are
handled at the same time and communication is limited.  In fact, 
one of the practical motivations for considering this problem
was estimating lightcurves for stars onboard the Kepler telescope.
In this case, sending the raw data for hundreds of thousands of
stars is significant. 

More importantly, we view the study of quantized estimation as a
theoretical problem of fundamental interest in its own right. Our hope
is that our ideas and techniques may illustrate a principled way to
study and quantify the tradeoff between statistical accuracy and
storage/communication resources, and lead to studies of more realistic
and complex models.

\rc{For such one dimensional problem, even
sending the raw data doesn't incur much communication cost. It is hard
to imagine that people want to compress the message from 100 bits to
30 bits using a complex algorithm just like this to save communication
cost.}

Figure 1 shows a surprising and potentially powerful result of our
analysis. The parameter $d$ can be considered to be the number of bits
per coefficient (on average), when coding at the minimax rate.  The plot shows that
with only 2 or 3 bits per coefficient, quantized
estimation shows effectively no degradation in the rate or constant
over classical, infinite precision estimation. In principle, this
could lead to huge savings in storage and communication costs.

\rc{The authors also mentioned distributed computing, but this paper
has nothing to do with distributed message passing. It assumes that
the data and the computation are centralized. Only the estimation
result needs to be quantized and communicated. I doubt if this is the
typical scenario that people meet in practice. The paper
will be improved if the authors study the distributed computing
setting, or analyze a more general family of models other than the
Gaussian white noise model.}

Distributed estimation and distributed quantization is out of the
scope of the current paper.  We feel it is better to focus on the
centralized setup in the current paper and leave the distributed
setting to future work. We have added a comment to this effect in the
introduction. In fact, we consider the white noise model to be a
fairly general and useful model of nonparametric regression.


\item \rc{For the upper bound, the authors assume that both agents
  have access to a random codebook. The size of this code book is
  exponential in $B$. I am not sure how practical this assumption
  is. First the computation cost of encoding and decoding is
  exponential in $B$, which might be over expensive. Second, it seems
  that the upper bound cannot be established without assuming this
  shared randomness. The upper bound is meaningful in the sense that
  it proves the tightness of the lower bound, but it doesn't provide a
  protocol that people can use in practice (because of the computation
  intractability). In particular, I am convinced that the lower holds
  under the shared randomness assumption. Thus in this setting the
  upper bound matches the lower bound. However, if the shared
  randomness is not allowed, then the authors have not proved that the
  lower bound is tight. The paper can be improved by proving it.}

In fact, the computation cost of encoding and decoding is exponential
in the largest number of bits allocated to each block, which is
smaller than $B$ but could still result in expensive computation. We
agree that this is a weakness of the procedure and would like to
improve it in future work.

Designing a protocol without the assumption of common randomness is in
some sense equivalent to finding a ``universal code,'' which may or
may not exist for this problem. We are currently not able to match the
lower bound up to constant without assuming common randomness between
the encoder and the decoder; an analysis outlined above, however,
matches the lower bound within a constant factor of 2.


\item \rc{I feel that Section 4 is difficult to parse. The presentation
should be imporved so that more intuitions are given for the
achievability result.  In particular, why
do you choose $T_k$ and $S_k$ by the formulas in the paper? There is another place that I don't
quite understand. Since $S_k$  
is the norm of $Y_{(k)}$, it is a random quantity. It means that the
quantities $b_k$ are also random.   Intuitively, if $S_k$ is large,
then $b_k$  should also be large to yield an
accurate approximation. Since the budget $B$ is fixed, the quantization
will lead to a bad approximation if many $S_k$'s are large. Probably this
happens with low probability, but I didn't get the intuition how this
issue was addressed.}

We have hopefully improved the readability of Section 4 by including
more intuition. On a high level, the procedure is to quantize the
minimax blockwise James-Stein estimator by allocating bits to each
block based on the signal to noise ratio.

When many $S_k$'s are large, the signal is relatively strong for the
parameter. The quantization might actually lead to a bad approximation
in this case as the procedure is adaptive, and this indicates that the
budget is not sufficient for the current parameter.

\end{enumerate}

\rc{Minor comments:}
\begin{enumerate}
\item \rc{In Example 2, the lower bound $R_n(\Theta, B_n) \geq  c_1
  +c_2 H^{-1}(1 - B_n/n)$ doesn't
seem to be correct. It means that to avoid a constant error rate, the communication cost
$B_n$ must be proportional to $n$. But since it suffices to send the sample average, the
commucation cost should scale as $\log(n)$ to achieve an $1/n$ error
rate.}

You are correct; we were using the customary distortion function given
by the relative Hamming distance. To avoid a digression,
we have removed this example in the revision, rather than rework it.

\item \rc{In page 24, the quantity $S_k$ has never been explicitly
  defined.}

This quantity is defined in step 2.1.

\item \rc{At the bottom of page 24, the ``argmin'' should be
  ``argmax''?}

Corrected.

\item \rc{In Figure 2, second row, the $b_1$ should be $b_2$.}

Corrected.

\end{enumerate}

\subsection*{Associate Editor}

\rc{Summary: The authors study the problem of estimation (under the white
noise model) over Sobolev ellipsoids, and study a constrained form of
the minimax risk, in which the number of bits is limited.  They
provide tight upper and lower bounds for their constrained risk, and
identify three regimes of bit constraints, corresponding to whether
the classical Pinsker result is matched exactly up to constants, up to
the correct order, or not achieved.  The problem is an interesting
one, and the paper is well-written overall.
\rskip
Three expert reviewers have looked over the paper.  Overall, they find
it interesting, and worth publishing, modulo some issues to be
addressed.
\rskip
A few suggestions for improvement:}
\begin{enumerate}[(1)]
\item \rc{It would help to provide some justification for studying the white
noise model.  The usual ones---i.e., based on the classical asymptotic
equivalence (Nussbaum, Brown, etc.) for density estimation and
regression---would be adequate.}

We have added a reference to the asymptotic equivalence with 
nonparametric regression (Brown and Low 1994), and made
the comment that it simplifies some of the mathematical analysis in
our framework.

\item \rc{The authors give a relatively complicated scheme for achieving
their lower bound.  It was not at all clear to me that a simpler
scheme would not suffice.  E.g., truncate the Fourier coefficients,
and then perform some type of block quantization on this vector.  If
this (or other simpler schemes) do not work, then the authors should
carefully explain why not.  If they do work, then I would suggest
modifying the achievability proof with the simpler procedure.}

This point has been addressed in the responses to Reviewer 1, with
the manuscript revised accordingly.

\item \rc{Equating computing resources to number of bits used: as noted by
Reviewer \#1, this point needs to be addressed more carefully.  Also,
the authors need to be clearer that they are talking specifically
about quantized minimax, as opposed to distributed estimation. (I.e.,
there are not multiple processors with communication between them, at
least in this paper.)}

This point has also been addressed in the responses to Reviewer 1, 
and in the manuscript.

\end{enumerate}

\vspace*{10pt}

Sincerely, 


Yuancheng Zhu and John Lafferty\\[1pt]
\today

\bibliography{local}
\end{document}

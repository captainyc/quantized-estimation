source("func.R")
#require(tikzDevice)
#options(tikzLatex="/Library/TeX/texbin/pdflatex")

# plot 1, examples for three sample sizes
dx = 1e-3
x = seq(0, 1, dx)
theta = fouriercoef(doppler, dx=dx, 1000)
theta0 = theta[1]
theta = theta[-1]
#tikz("simulation_example.tex",height=5,width=4)
layout(matrix(c(rep(1:3,each=3),4), ncol=1))
ns = c(500, 5000, 50000)
for (n in ns) {
	set.seed(1)
	eps = 1/sqrt(n)
	z = theta + eps*rnorm(length(theta))
	z.bjs = bjs(z, eps, 2/3)
	y.bjs = invfourier(c(theta0, z.bjs), dx)
	z.qne5 = qne(z, eps, 5)
	y.qne5 = invfourier(c(theta0, z.qne5), dx)
	z.qne30 = qne(z, eps, 30)
	y.qne30 = invfourier(c(theta0, z.qne30), dx)
	par(mar=c(3,4,2,1))
	plot(x, doppler(x), "l", ylab="$y$", xlab="", ylim=c(-0.6, 0.6), col="gray", axes=FALSE)
	mtext(paste0("$n=",n,"$"), side=1, cex=0.7, line=2.5)
	axis(1)
	axis(2, at=seq(-0.6,0.6,0.3))
	lines(x, y.bjs, lty=2)
	lines(x, y.qne5, col="red", lty=3)
	lines(x, y.qne30, col="blue", lty=4)
}
par(mar=c(0, 4, 0, 1))
plot(c(0,1), c(-0.6,0.6), "n", axes=FALSE, xlab="", ylab="")
legend("center", legend=c("true function", "blockwise J-S", "5 bits", "30 bits"), lty=1:4, col=c("gray", "black", "red", "blue"), bty="n", horiz=TRUE, xjust=0.5, yjust=0.5)
#dev.off()

# plot 2, convergence rate
load("risk_doppler.RData")
ns = floor(exp(seq(log(1e2),log(1e4),0.1)))
bits = c(5,10,15,20,25,30)
#tikz("simulation_rate.tex",height=6,width=4.5)
layout(matrix(c(rep(1:2,each=3),3), ncol=1))
par(mar=c(4, 4, 2, 1))
plot(ns, colMeans(risk.bjs), "l", xlab="Effective sample size $n$", ylab="Risk", lty=2, xlim=c(0,1e4), ylim=c(0,0.1), axes=FALSE)
axis(1, at=seq(0, 1e4, 2500))
axis(2, at=seq(0, 0.1, 0.025), las=2)
color = colorRampPalette(c("red", "orange"))(length(bits))
for (i in 1:length(bits)) {
	lines(ns, colMeans(risk.qne[[i]]), col=color[i])
}
par(mar=c(4, 4, 2, 1))
plot(ns, colMeans(risk.bjs), "l", xlab="Effective sample size $n$", ylab="Risk", lty=2, xlim=c(1e2,1e4), ylim=c(0.0025,0.1), axes=FALSE, log="xy")
axis(1, at=c(1e2, 1e3, 1e4))
axis(2, at=c(0.001, 0.01, 0.1), las=2)
color = colorRampPalette(c("red", "orange"))(length(bits))
for (i in 1:length(bits)) {
	lines(ns, colMeans(risk.qne[[i]]), col=color[i])
}
par(mar=c(0, 4, 0, 1))
plot(c(0,2), c(0,2), "n", axes=FALSE, xlab="", ylab="")
legend(1, 1, legend=c("blockwise J-S"), lty=2, col="black", bty="n", horiz=TRUE, xjust=0.5, yjust=0)
legend(1, 1, legend=c("5 bits", "10 bits", "15 bits", "20 bits", "25 bits", "30 bits"), lty=rep(1, 6), col=color, bty="n", horiz=TRUE, xjust=0.5, yjust=1)
#dev.off()
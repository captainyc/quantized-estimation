- func.R contains functions for constructing models and estimators.
- main.R carries out simulations comparing the blockwise James-Stein estimator
         and quantized estimator with various storage/communication budgets.
- plot.R generates the two sets of plots appearing in the paper.
